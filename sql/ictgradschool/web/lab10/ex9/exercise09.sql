-- Answers to Exercise 9 here
SELECT * FROM ex_03;

SELECT id,name, gender, year_born, joined FROM ex_03;

SELECT title FROM ex_04;

SELECT DISTINCT name FROM  ex_03;

SELECT movie_title FROM ex_06 WHERE weekly_rate < 2;

SELECT * FROM loan;

SELECT * FROM loan ORDER BY customer_name DESC;

SELECT * FROM ex_03 WHERE name LIKE 'Pete%';

SELECT * FROM ex_05 WHERE first_name LIKE 'B%' OR last_name LIKE 'B%';