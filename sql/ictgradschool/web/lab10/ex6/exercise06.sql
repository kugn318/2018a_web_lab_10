-- Answers to Exercise 3 here

CREATE TABLE IF NOT EXISTS loan (
  loan_id INT NOT NULL,
  customer_name TEXT,
  PRIMARY KEY (loan_id)
);

DROP TABLE ex_06;

CREATE TABLE IF NOT EXISTS ex_06 (
  id INT NOT NULL,
  loan_id2 INT NOT NULL,
  movie_title VARCHAR(20),
  directed VARCHAR(20),
  weekly_rate INT,
  PRIMARY KEY (id),
  FOREIGN KEY (loan_id2) REFERENCES loan (loan_id)
);

INSERT INTO loan VALUES (1, 'cName1');
INSERT INTO loan VALUES (2, 'cName1');
INSERT INTO loan VALUES (3, 'cName1');
INSERT INTO loan VALUES (4, 'cName1');
INSERT INTO loan VALUES (5, 'cName6');
INSERT INTO loan VALUES (6, 'cName3');




INSERT INTO ex_06 VALUES (1, 1, 'title1', 'director123', 2);
INSERT INTO ex_06 VALUES (2, 2, 'title2', 'director123', 4);
INSERT INTO ex_06 VALUES (3, 3, 'title1', 'director', 6);
INSERT INTO ex_06 VALUES (4, 4, 'title4', 'director1234', 1);


SELECT * FROM ex_06;
SELECT * FROM loan;
