-- Answers to Exercise 5 here
CREATE TABLE IF NOT EXISTS ex_05 (
  username VARCHAR(20) NOT NULL,
  first_name VARCHAR(20),
  last_name VARCHAR(20),
  email VARCHAR(20),
  PRIMARY KEY (username)
);

INSERT INTO ex_05 VALUES ('programmer5', 'Bill', 'Gates', 'bill@microsoft.com');
INSERT INTO ex_05 VALUES ('programmer6', 'Bill6', 'Gates', 'bill@microsoft.com');
INSERT INTO ex_05 VALUES ('programmer7', 'Bill7', 'Gates', 'bill@microsoft.com');

-- INSERT INTO ex_05 VALUES ('programmer5', 'Bill', 'Gates', 'bill@microsoft.com');

SELECT * FROM ex_05;