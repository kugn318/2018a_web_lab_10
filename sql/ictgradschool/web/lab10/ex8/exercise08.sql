-- Answers to Exercise 8 here

-- delete a row
DELETE FROM ex_05 WHERE username = 'programmer6';
SELECT * FROM ex_05;

-- Delete a column
ALTER TABLE ex_05 DROP COLUMN email;
SELECT * FROM ex_05;

-- delete the entire table
DROP TABLE ex_05;

-- update

UPDATE ex_05 SET first_name = 'blah blah' WHERE username = 'programmer5';

