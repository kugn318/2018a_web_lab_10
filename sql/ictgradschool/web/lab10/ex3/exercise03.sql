-- Answers to Exercise 3 here
CREATE TABLE IF NOT EXISTS ex_03 (
  id INT NOT NULL,
  name VARCHAR(20),
  gender VARCHAR(6),
  year_born INT,
  joined INT,
  num_hires INT,
  PRIMARY KEY (id)
);

INSERT INTO ex_03 VALUES (1, 'Peter Jackson', 'male', 1961, 1997, 17000);
INSERT INTO ex_03 VALUES (2, 'Jane Campion', 'female', 1954, 1980, 30000 );
INSERT INTO ex_03 VALUES (3, 'Roger Donaldson ', 'male', 1945, 1980, 12000 );
INSERT INTO ex_03 VALUES (4, 'Temuera Morrison', 'male', 1960, 1995, 15500 );
INSERT INTO ex_03 VALUES (5, 'Temuera Morrison', 'male', 1964, 1990, 10000 );


SELECT * FROM ex_03;