DROP TABLE comments;
DROP TABLE ex_07;


CREATE TABLE IF NOT EXISTS ex_07 (
  article_id INT NOT NULL AUTO_INCREMENT,
  title TEXT,
  text TEXT,
  PRIMARY KEY (article_id)
);

CREATE TABLE IF NOT EXISTS comments (
  article_id INT NOT NULL AUTO_INCREMENT,
  public_comments TEXT,
  FOREIGN KEY (article_id) REFERENCES ex_07 (article_id)
);



INSERT INTO ex_07 (title, text) VALUES ('Lorem Ipsum', 'text 1');
INSERT INTO ex_07 (title, text) VALUES ('Lorem Ipsum2', 'text 2');

INSERT INTO comments (public_comments) VALUES ('comment1');
INSERT INTO comments (public_comments) VALUES ('comment2');



SELECT * FROM ex_07;
SELECT * FROM comments;

