-- Answers to Exercise 2 here
CREATE TABLE IF NOT EXISTS ex_02 (
  username VARCHAR(20) NOT NULL,
  first_name VARCHAR(20),
  last_name VARCHAR(20),
  email VARCHAR(20),
  PRIMARY KEY (username)
);

INSERT INTO ex_02 VALUES ('programmer1', 'Bill', 'Gates', 'bill@microsoft.com');

SELECT * FROM ex_02;